﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Task_Varyence.Models;

namespace Task_Varyence.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        async public Task<ActionResult> Submit(IFormCollection fc)
        {
            Uri uriResult;
            bool result = Uri.TryCreate(fc["Url"], UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            if (result)
            {
                ViewBag.Url = uriResult;
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage httpResponseMessage = await httpClient.GetAsync(uriResult);
                HttpContent httpContent = httpResponseMessage.Content;
                string page_content = await httpContent.ReadAsStringAsync();
                ViewBag.Content = page_content;
            }
            else
            {
                ViewBag.Url = "You entered invalid url. Check it and try again!";
                ViewBag.Content = "Seems like it`s empty";
            }
           
            return View("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
