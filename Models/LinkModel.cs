﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_Varyence.Models
{
    public class LinkModel
    {
        public string Url { get; set; }

        public LinkModel() { }
        public LinkModel(string url)
        {
            Url = url;
        }
    }
}
